# DnD Worldbuilding
## Orsia
### Charactere
#### Yayx
Beschreibung:  
*Größe*: 5'7".  
*Aussehen*: Kupferfarbene Haut, dunkles, lila schimmerndes geflochtenes langes Haar, schmächtig.  
*Eigenschaften (personality traits)*: Beobachtet gerne Tiere, Mehr loyal als pazifistisch.  
*Ideale (ideals)*: Natur über allem.  
*Verbindungen (bonds)*: Die alten Wälder.  
*Schwachstellen*: Furcht vor Feuer.  
*Thematiken*: Mond, Kälte, Spinnen.  
##### Leben
*Im Jahr 601 aX* (0 Jahre alt):  
Geboren in einem kleinen Dorf namens *Mihin* von Waldelfen in einem der "ancient forests": *Moern krey* in Finia im fernen Osten des Kontinents Orsia. Die Eltern sind in unserem Dorf den restlichen Bewohner gleichgestellt, alle sorgen für die Kinder. Das Dorf ist abgeschieden, nur andere Waldelfen können das Dorf besuchen oder sich dort niederlassen. Die Dorfbewohner gehen aber auch auf Reisen und lernen so die restliche Welt kennen. Die Dorfbewohner leben genügsam und im Einklang mit der sie umgebenden Natur und sind den Expansionsbestrebungen anderer Völker entsprechend kritisch gegen über eingestellt. Mit Sorge betrachten sie wie viele Wälder durch die Expansionen schrumpfen oder ganz zum Opfer fallen. Die Ausbreitung beschleunigt sich sogar in Richtung ihres Heimatwaldes. Die Stadt *Rigidie* wächst immer weiter.  
*Im Jahr 604 aX* (3 Jahre alt):  
In jungem Alter wird den Elfenkindern verschiedenes Spielzeug zur Auswahl gegeben. Anhand dessen können die Dorfältesten die Grundfähigkeiten der Kinder erkennen. In ihrer Jugend werden sie dann entsprechend ausgebildet. Meine Wahl fiel auf eine Puppe einer Spinne. Darin haben die Ältesten meine Zukunft als Druide erkannt.   
*Im Jahr 611 aX* (10 Jahre alt):  
Kinder haben zunächst keinen Kontakt zur Außenwelt, nur über Geschichten von Reisenden. In Erzählungen und Märchen wird auch immer die Gefahr von Naturzerstörung thematisiert und wie wichtig Genügsamkeit ist.  
*Im Jahr 614 aX* (13 Jahre alt):  
In unserem Wald lebt schon seit langer Zeit ein mystischer großer brauner Bär. Alle Dorfbewohner kennen den Bären und die Druiden unter ihnen besonders. Die Verwandlung in diesen Bären ist ein Markenzeichen der Druiden unseres Dorfes. So wurde es dann auch bei mir Zeit, mich im Rahmen meiner Ausbildung mit dem Bären zu beschäftigen. Mein Mentor *Karacx* hat diesen Bären vor 500 Jahren kennengelernt und kann sich in den mächtigsten Bären im Dorf verwandeln. Im Laufe meiner Jungend habe auch ich den Bären sehr gut kennengelernt.  
*Im Jahr 681 aX* (80 Jahre alt):  
Menschenstädte wachsen in Richtung unseres Waldes. Erste diplomatische Missionen vom Dorf versuchen ein mögliches Zusammenleben auszuhandeln. Ihre Forderungen sind eher Radikal, sodass sich die Gegenseite uneinsichtig zeigt.  
*Im Jahr 684 aX* (83 Jahre alt):  
[TODO: Erster Zusammenstoß einiger Dorfbewohner mit *Marod & Co.*]  
*Im Jahr 696 aX* (95 Jahre alt):  
Mit der Zeit beginnen die Dorfbewohner mit Sabotageakten. Die Städter lassen sich jedoch nicht davon beeindrucken.  
*Im Jahr 706 aX* (105 Jahre alt):  
Ich bin erwachsen! Ich lege meinen Kindernamen Kyri ab und nenne mich Yayx, nach einer alten Legende vom Spinnenkönig Yayx. Jungen Erwachsenen Dorfbewohnern wird empfohlen zu reisen um die Welt außerhalb kennen zu lernen und andere Perspektiven zu gewinnen.  
*Im Jahr 716 aX* (115 Jahre alt):  
In einer großen Stadt erfahre ich, dass dort einmal ein ancient forest *Moern dasah* stand. Ich lerne Nachfahren der dort heimischen Waldelfen kennen. Ich erfahre auch, dass auch in dieser Stadt *Marod & Co.* maßgeblich an dieser Entwicklung beteiligt sind.  
*Im Jahr 723 aX* (122 Jahre alt):  
Ich begegne auf meinen Reisen durch die Wälder im Süden des Kontinents einer riesigen Spinne wie ich noch nie eine gesehen habe. Da ich Spinnen grundsätzlich interessant finde, beschließe ich, sie für eine längere Zeit zu verfolgen und beobachten und lerne dabei alle ihre Verhaltensweisen kennen. In dieser Zeit lerne ich einen sympathischen Gnom namens *Memo* kennen, nur ein Jahr junger ist als ich. Ich erzähle ihm von meinem aktuellen Studienobjekt und erfahre, dass er meine Begeisterung teilt. Allerdings für seltene/besondere Lebewesen im Allgemeinen statt im Besonderen für Spinnen. In der nächsten Zeit beobachten wir die Spinne zusammen und verbringen viel Zeit miteinander. Ich bringe ihn ein paar Druidenskills bei und er mir Zwergisch, da er in einer Zwergenstadt aufgewachsen ist. Nach einigen Monaten trennen wir uns von der Spinne, reisen aber noch viele Jahre zusammen durch den Süden.  
*Im Jahr 727 aX* (126 Jahre alt):  
Im Tiefen Süden begegnen wir auch Löwen von denen ich vorher noch nie was gehört hatte. Das Leben eines großen Tieres den größeren Gemeinschaftsbund war mir bis jetzt fremd. Deshalb beschließen wir, auch die Löwen besonders zu studieren.  
*Im Jahr 741 aX* (140 Jahre alt):  
Ich kehre zurück ins Dorf. Das Dorf hat sich zunehmend radikalisiert. Ich will nicht meine Heimat verlieren und kämpfe mit.  
*Im Jahr 771 aX* (170 Jahre alt):  
Die Erfolge sind geringe, die Mittel auf beiden Seiten eskalieren, der Wald wird abgeholzt.  
*Im Jahr 786 aX* (185 Jahre alt):  
Menschen und Elfen sterben, Menschen nutzen Feuer, große Waldbrände. Die Schrecken des Feuers gehen mir seitdem nicht mehr aus dem Kopf. Ich meide die Hitze des Feuers und bevorzuge Kälte.  
*Im Jahr 801 aX* (200 Jahre alt):  
Ich sehe keinen Sinn in der weiteren Eskalation. Ältere sehen keinen aber keinen anderen Weg. Im Dorf wird viel diskutiert aber die Mehrheit ist eher militant eingestellt. Ich mache mich wieder auf den Weg in die Welt um sie zu verstehen. Um die Expansionsgesellschaft von innen zu erleben, betrachten und zu verstehen beschließe ich verschiedene Stadtberufe anzunehmen und so verschiedene Erfahrungen zu sammeln. Carax ist wie alle anderen uneinsichtig und militant, begrüßt es aber, dass ich andere Wege Suche und wünscht mir das beste für meine Reisen.  
*Im Jahr 804 aX* (203 Jahre alt):  
Ausbildung als Bauarbeiter.  
*Im Jahr 809 aX* (208 Jahre alt):  
Ausbildung als [TODO..., vll noch iwas Schreiberling-mäßiges. Auf jeden Fall noch eine ganze Reihe von Berufen]  
*Im Jahr 855 aX* (254 Jahre alt):  
Anfang als Lehrling in einer Brauerei in *Greendawn*.  
#### Andere PCs
*Ivan*: Zwerg Fighter (gespielt von Nico).  
*Kargan*: Zwerg Fighter (gespielt von Vincent), getötet im Jahr 856 aX.  
*Melaran*: Mensch Cleric (gespielt von Vincent) (2. char).  
*Bjorke*: Hochelf Sorcerer (gespielt von Clemens).  
*Paelias*: Waldelf Rogue (gespielt von Robin).  
#### NPCs
*Memo Schirin*: Geboren: 602 aX. Gnom Ranger. Aufgewachsen in der Zwergenstadt *Dhomhall* im Süden des Kontinents.  
*Karacx Karaxis*: Geboren: 86 aX. Waldelf Druide. Druidenmentor von Yayx.  
*Familie Marod/Marod & Co.*: Familie von Menschen, die ein Bau-/Entwicklungsunternehmen aufgebaut haben. Starker Glaube daran, dass die Zukunft der Menschen in den Städten liegt. Ihren Platz in dieser Zukunft will die Familie mit ihrem Unternehmen sichern.  
*Thorgil*: aktueller (856 aX) Duergarenkönig.  
### Orte
*Orsia*: Kontinent, dem die Geschichte spielt.  
*Mihin*: Heimatdorf von Yayx. Versteckt im Wald *Moern krey*.  
*Moern krey*: "ancient forest", Heimat von Yayx. Liegt im Land Finia im fernen Osten des Kontinents Orsia in der Nähe der Stadt *Rigidie*. Gemäßigtes Klima.  
*Rigidie*: Stadt im Westen von *Moern krey*, die unter anderem immer weiter in Richtung des Waldes wächst.  
*Bosia*: Land im Nordwesten *Orsia*s.  
*Greendawn*: Hauptstadt von *Bosia*. Hafenstadt an der Nordküste von *Orsia*. Ort der Brauereiausbildung von Yayx.  
*Brina*: Stadt in *Bosia* südlich von *Greendawn*.  