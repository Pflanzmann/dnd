
import jsoncfg as json

def main():

	SOURCE="content.json"
	TARGET="README.md"



	title="# DnD Worldbuilding"


	with open(SOURCE,"r") as f:
		data=json.loads(f.read())

	text=[title]
	
	for l_name,l_content in data["legends"].items():
		text.append(f"## {l_name}")
		text.append(f"### Charactere")
		for c_name,c_content in l_content["my_pcs"].items():
			text.append(f"#### {c_name}")
			text.append(f"Beschreibung:  ")
			for d_name,d_content in c_content["description"].items():
				if isinstance(d_content,str):
					text.append(f"*{d_name}*: {d_content}.  ")
				else:
					text.append(f"*{d_name}*: "+(", ".join(d_content))+".  ")
			text.append("##### Sessions")
			for session in c_content["sessions"]:
				text.append(f"*Session {session['id']}* am {session['date']} \"{session['title']}\":  ")
				text.append(session["events"]+"  ")
			text.append("##### Leben")
			for entry in c_content["life"]:
				text.append(f"*Im Jahr {entry['year']} {entry['era']}* ({entry['age']} Jahre alt):  ")
				text.append(entry["events"]+"  ")
				
		text.append(f"#### Andere PCs")
		for c_name,c_content in l_content["other_pcs"].items():
			text.append(f"*{c_name}*: {c_content}.  ") 
		text.append(f"#### NPCs")
		for c_name,c_content in l_content["npcs"].items():
			text.append(f"*{c_name}*: {c_content}.  ")
		text.append(f"### Orte")
		for loc_name,loc_content in l_content["locations"].items():
			text.append(f"*{loc_name}*: {loc_content}.  ") 



					
	with open(TARGET,"w") as f:
		f.write("\n".join(text))

def trash():



	table_headers_from_name={
		"Leben":("Alter","Geschehnisse"),
		"Tagebuch":("Session","Geschehnisse"),
		"Charactere":("Name","Info"),
		"Orte":("Name","Info"),
	}


	# chapters=sorted([k for k in data.keys()])
	chapters=sorted(data.keys())
	for chapter in chapters:
		chapter_name=chapter.split(" ",1)[1]
		hl,hr=table_headers_from_name[chapter_name]
		text.append(f"## {chapter}\n{hl}|{hr}\n-|-")
		subdata=data[chapter]
		keys=subdata.keys()
		try:
			sorted_keys=[str(j) for j in sorted([int(k) for k in keys])]
		except:
			sorted_keys=sorted(keys)
		for key in sorted_keys:
			text.append(f"{key}|{subdata[key]}")


main()
